package com.garden;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.web.bind.annotation.GetMapping;


import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@EnableEurekaServer
public class SpringbootApplication {

	public static void main(String[] args) {
			SpringApplication.run(SpringbootApplication.class, args);
	}

}
